package com.edu.softserve.mymovie.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.edu.softserve.mymovie.R;
import com.edu.softserve.mymovie.adapters.ListMovieAdapter;
import com.edu.softserve.mymovie.models.Movie;
import com.edu.softserve.mymovie.models.MovieList;
import com.edu.softserve.mymovie.network.MovieDBService;
import com.edu.softserve.mymovie.repositories.MovieRepository;
import com.edu.softserve.mymovie.repositories.MovieRepositoryImpl;

import java.util.Collections;
import java.util.List;

import io.realm.Realm;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView movieList;
    private Button buttonNext;
    private Button buttonPrev;
    private List<Movie> films = Collections.emptyList();
    private int pageNumber = 1;
    private int favPageNumber = 1;
    private int totalPages;
    private boolean favorite = false;
    private MovieRepository movieRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);

        movieRepository = new MovieRepositoryImpl(Realm.getDefaultInstance());

        movieList = findViewById(R.id.movie_list);
        buttonNext = findViewById(R.id.button_next);
        buttonPrev = findViewById(R.id.button_previous);

        movieList.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigate(0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Movie movie = (Movie) parent.getItemAtPosition(position);
        Long movieId = movie.getId();

        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.MOVIE_ID, movieId);
        startActivity(intent);
    }

    public void loadPopularMovies(int page) {
        final Call<MovieList> call = MovieDBService.movieEndpoint.getPopular(getString(R.string.api_key), page);

        call.enqueue(new Callback<MovieList>() {
            @Override
            public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                if (response.isSuccessful() && response.body() != null) {
                    films = response.body().getResults();
                    totalPages = response.body().getTotalPages();
                    refreshNavButtons();
                    ListMovieAdapter adapter = new ListMovieAdapter(getApplicationContext(), R.layout.list_of_movies, films);
                    movieList.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "ERROR occurred by response body " + response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MovieList> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR OCCURRED " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void loadFavoriteMovies(int page) {

        if (isNetworkAvailable()) {
            final Call<MovieList> call = MovieDBService.movieEndpoint.getFavorite(getString(R.string.api_key), getString(R.string.session_id), page);

            call.enqueue(new Callback<MovieList>() {
                @Override
                public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        films = response.body().getResults();
                        totalPages = response.body().getTotalPages();
                        refreshNavButtons();
                        ListMovieAdapter adapter = new ListMovieAdapter(getApplicationContext(), R.layout.list_of_movies, films);
                        movieList.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR occurred by response body " + response.message(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<MovieList> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "ERROR OCCURRED " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            List<Movie> allMoviesFromDB = movieRepository.getAllMovies();
            ListMovieAdapter adapter = new ListMovieAdapter(getApplicationContext(), R.layout.list_of_movies, allMoviesFromDB);
            movieList.setAdapter(adapter);
            Toast.makeText(getApplicationContext(), "realm movies loaded, " + allMoviesFromDB.size(), Toast.LENGTH_LONG).show();
        }
    }

    public void buttonNextClick(View view) {
        navigate(1);
        refreshNavButtons();
    }

    public void buttonPrevClick(View view) {
        navigate(-1);
        refreshNavButtons();
    }

    public void onCheckboxClick(View view) {
        if (view instanceof AppCompatCheckBox) {
            AppCompatCheckBox checkBox = (AppCompatCheckBox) view;
            favorite = checkBox.isChecked();
        }
        navigate(0);
        refreshNavButtons();
    }

    private void refreshNavButtons() {
        if (favorite) {
            buttonPrev.setEnabled(favPageNumber > 1);
            buttonNext.setEnabled(favPageNumber < totalPages);
        } else {
            buttonPrev.setEnabled(pageNumber > 1);
            buttonNext.setEnabled(pageNumber < totalPages);
        }
    }

    private void navigate(int i) {
        if (favorite) {
            loadFavoriteMovies(favPageNumber += i);
        } else {
            loadPopularMovies(pageNumber += i);
        }
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}