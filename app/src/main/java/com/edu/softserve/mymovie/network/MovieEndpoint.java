package com.edu.softserve.mymovie.network;

import com.edu.softserve.mymovie.models.FavoriteMovie;
import com.edu.softserve.mymovie.models.Movie;
import com.edu.softserve.mymovie.models.MovieList;
import com.edu.softserve.mymovie.models.ResponseStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieEndpoint {

    @GET("movie/popular?")
    Call<MovieList> getPopular(@Query("api_key") String apiKey, @Query("page") int page);

    @GET("movie/{movie_id}?")
    Call<Movie> getDetails(@Path("movie_id") Long movieId, @Query("api_key") String apiKey);

    @POST("account/{account_id}/favorite?")
    Call<ResponseStatus> markAsFavorite(@Query("api_key") String apiKey, @Query("session_id") String sessionId, @Body FavoriteMovie favoriteMovie);

    @GET("account/{account_id}/favorite/movies?")
    Call<MovieList> getFavorite(@Query("api_key") String apiKey, @Query("session_id") String sessionId, @Query("page") int page);
}