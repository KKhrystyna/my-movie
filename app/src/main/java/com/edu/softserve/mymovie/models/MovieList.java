package com.edu.softserve.mymovie.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class MovieList {
    @SerializedName("page")
    private Integer page;

    @SerializedName("total_results")
    private Integer totalResults;

    @SerializedName("total_pages")
    private Integer totalPages;

    @SerializedName("results")
    private List<Movie> results;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieList movieList1 = (MovieList) o;
        return Objects.equals(page, movieList1.page) &&
                Objects.equals(totalResults, movieList1.totalResults) &&
                Objects.equals(totalPages, movieList1.totalPages) &&
                Objects.equals(results, movieList1.results);
    }

    @Override
    public int hashCode() {
        return Objects.hash(page, totalResults, totalPages, results);
    }

    @Override
    public String toString() {
        return "MovieList{" +
                "page=" + page +
                ", totalResults=" + totalResults +
                ", totalPages=" + totalPages +
                ", movieList=" + results +
                '}';
    }
}