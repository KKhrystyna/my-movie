package com.edu.softserve.mymovie.repositories;

import com.edu.softserve.mymovie.models.Movie;

import java.util.List;

public interface MovieRepository {
    void addMovie(Movie movie);

    void deleteMovieById(Long id);

    List<Movie> getAllMovies();

    Movie findById(Long id);
}