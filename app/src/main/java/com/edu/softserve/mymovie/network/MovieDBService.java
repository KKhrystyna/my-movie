package com.edu.softserve.mymovie.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDBService {
    public static String REST_END_POINT = "https://api.themoviedb.org/3/";

    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(REST_END_POINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static MovieEndpoint movieEndpoint = retrofit.create(MovieEndpoint.class);
}