package com.edu.softserve.mymovie.models;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class ResponseStatus {
    @SerializedName("status_code")
    private Long statusCode;

    @SerializedName("status_message")
    private String statusMessage;

    public Long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseStatus that = (ResponseStatus) o;
        return Objects.equals(statusCode, that.statusCode) &&
                Objects.equals(statusMessage, that.statusMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statusCode, statusMessage);
    }

    @Override
    public String toString() {
        return "ResponseStatus{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}