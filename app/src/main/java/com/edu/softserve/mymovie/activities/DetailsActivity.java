package com.edu.softserve.mymovie.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.softserve.mymovie.R;
import com.edu.softserve.mymovie.models.FavoriteMovie;
import com.edu.softserve.mymovie.models.Movie;
import com.edu.softserve.mymovie.models.ResponseStatus;
import com.edu.softserve.mymovie.network.MovieDBService;
import com.edu.softserve.mymovie.repositories.MovieRepository;
import com.edu.softserve.mymovie.repositories.MovieRepositoryImpl;
import com.squareup.picasso.Picasso;

import io.realm.Realm;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edu.softserve.mymovie.adapters.ListMovieAdapter.REST_IMAGE_POINT;

public class DetailsActivity extends AppCompatActivity {

    private ImageView posterPath = null;
    private TextView title = null;
    private TextView overview = null;
    private TextView releaseDate = null;
    private TextView popularity = null;
    private CheckBox checkBoxFavorite = null;
    public final static String MOVIE_ID = "movieId";
    private long movieId;
    private MovieRepository movieRepository;
    private Movie currentMovie = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_details_landscape);
        } else {
            setContentView(R.layout.activity_details);
        }

        movieRepository = new MovieRepositoryImpl(Realm.getDefaultInstance());

        Intent intent = getIntent();
        movieId = intent.getLongExtra(MOVIE_ID, 0);
        posterPath = findViewById(R.id.poster_path);
        title = findViewById(R.id.title_name);
        overview = findViewById(R.id.overview);
        releaseDate = findViewById(R.id.release_date);
        popularity = findViewById(R.id.popularity);
        checkBoxFavorite = findViewById(R.id.check_box);

        loadMovieDetails(movieId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkBoxFavorite.setChecked(movieRepository.findById(movieId) != null);
    }

    public void loadMovieDetails(Long movieId) {
        Call<Movie> movieCallable = MovieDBService.movieEndpoint.getDetails(movieId, getString(R.string.api_key));

        movieCallable.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Picasso.get().load(REST_IMAGE_POINT + response.body().getPosterPath()).into(posterPath);
                    currentMovie = response.body();
                    title.setText(response.body().getTitle());
                    overview.setText(response.body().getOverview());
                    releaseDate.setText(response.body().getReleaseDate());
                    popularity.setText(String.valueOf(response.body().getPopularity()));
                } else {
                    Toast.makeText(getApplicationContext(), "ERROR OCCURRED " + response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR OCCURRED", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void markAsFavorite(View view) {
        AppCompatCheckBox checkBox = (AppCompatCheckBox) view;

        final boolean checked;
        checked = checkBox.isChecked();

        FavoriteMovie favoriteMovie = new FavoriteMovie();
        favoriteMovie.setMediaId(movieId);
        favoriteMovie.setFavorite(checked);
        Call<ResponseStatus> movieCallable = MovieDBService.movieEndpoint.markAsFavorite(getString(R.string.api_key), getString(R.string.session_id), favoriteMovie);
        movieCallable.enqueue(new Callback<ResponseStatus>() {
            @Override
            public void onResponse(Call<ResponseStatus> call, Response<ResponseStatus> response) {
                if (checked) {
                    movieRepository.addMovie(currentMovie);
                } else {
                    movieRepository.deleteMovieById(currentMovie.getId());
                }
                Toast.makeText(getApplicationContext(), response.body().getStatusMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<ResponseStatus> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }
}