package com.edu.softserve.mymovie.models;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class FavoriteMovie {
    @SerializedName("media_type")
    private String mediaType = "movie";

    @SerializedName("media_id")
    private Long mediaId;

    @SerializedName("favorite")
    private Boolean favorite;

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public Long getMediaId() {
        return mediaId;
    }

    public void setMediaId(Long mediaId) {
        this.mediaId = mediaId;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FavoriteMovie that = (FavoriteMovie) o;
        return Objects.equals(mediaType, that.mediaType) &&
                Objects.equals(mediaId, that.mediaId) &&
                Objects.equals(favorite, that.favorite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mediaType, mediaId, favorite);
    }

    @Override
    public String toString() {
        return "FavoriteMovie{" +
                "mediaType='" + mediaType + '\'' +
                ", mediaId=" + mediaId +
                ", favorite=" + favorite +
                '}';
    }
}