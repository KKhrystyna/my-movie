package com.edu.softserve.mymovie.repositories;

import com.edu.softserve.mymovie.models.Movie;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MovieRepositoryImpl implements MovieRepository {

    private Realm realm;

    public MovieRepositoryImpl(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void addMovie(Movie movie) {
        if (findById(movie.getId()) != null) {
            return;
        }

        realm.beginTransaction();

        Movie m = realm.createObject(Movie.class);
        m.setId(movie.getId());
        m.setPosterPath(movie.getPosterPath());
        m.setTitle(movie.getTitle());
        m.setOverview(movie.getOverview());
        m.setReleaseDate(movie.getReleaseDate());
        m.setPopularity(movie.getPopularity());

        realm.commitTransaction();
    }

    @Override
    public void deleteMovieById(Long id) {
        if (findById(id) == null) {
            return;
        }
        realm.beginTransaction();
        Movie movie = realm.where(Movie.class).equalTo("id", id).findFirst();
        movie.deleteFromRealm();
        realm.commitTransaction();
    }

    @Override
    public List<Movie> getAllMovies() {
        RealmResults results = realm.where(Movie.class).findAll();
        List<Movie> list = realm.copyFromRealm(results);

        return list;
    }

    public Movie findById(Long id) {
        return realm.where(Movie.class).equalTo("id", id).findFirst();
    }
}