package com.edu.softserve.mymovie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edu.softserve.mymovie.R;
import com.edu.softserve.mymovie.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListMovieAdapter extends ArrayAdapter<Movie> {
    public static String REST_IMAGE_POINT = "https://image.tmdb.org/t/p/w500";

    public ListMovieAdapter(Context context, int resource, List<Movie> movies) {
        super(context, resource, movies);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_of_movies, null);
        }

        Movie movie = getItem(position);

        if (movie != null) {
            ImageView image = v.findViewById(R.id.poster_path);
            TextView title = v.findViewById(R.id.title_name);

            if (image != null) {
                Picasso.get().load(REST_IMAGE_POINT + movie.getPosterPath()).into(image);
            }

            if (title != null) {
                title.setText(movie.getTitle());
            }
        }
        return v;
    }
}